<h1>Contact</h1>
<p>
For general questions about the use of Squeeze, check the <a href="http://forum.xfce.org">Xfce forum</a> or the <a href="http://foo-projects.org/mailman/listinfo/xfce/">Xfce mailinglist</a>.<br/>
</p>

<p>
Found bugs?<br/>
Report them at the <a href="http://bugzilla.xfce.org"/>Xfce bugzilla</a>,
or the <a href="http://foo-projects.org/mailman/listinfo/xfce4-dev/">Xfce4-dev mailinglist</a>.
</p>

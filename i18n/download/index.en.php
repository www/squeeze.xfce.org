<h1>Download</h1>

<h3>Stable Version</h3>
<p>
You can download tarballs of the latest stable release here:<br/>
</p>
<h4>Source Code</h4>
<p>
<ul>
<li><a href="/downloads/squeeze-0.2.3.tar.bz2">Squeeze 0.2.3</a>&nbsp;
<a href="/downloads/ChangeLog-0.2.3">ChangeLog</a></li>
<li><a href="/downloads/squeeze-0.2.2.tar.bz2">Squeeze 0.2.2</a>&nbsp;
<a href="/downloads/ChangeLog-0.2.2">ChangeLog</a></li>
<li><a href="/downloads/squeeze-0.2.1.tar.bz2">Squeeze 0.2.1</a>&nbsp;
<a href="/downloads/ChangeLog-0.2.1">ChangeLog</a></li>
<li><a href="/downloads/squeeze-0.2.0.tar.bz2">Squeeze 0.2.0</a>&nbsp;
<a href="/downloads/ChangeLog-0.2.0">ChangeLog</a></li>
<li><a href="/downloads/squeeze-0.1.0.tar.bz2">Squeeze 0.1.0</a></li>
</ul>
</p>
<hr/>
<h3>SVN Snapshots</h3>
<p>
You can download the latest SVN snapshot <a href="http://mocha.xfce.org/downloads/svn-snapshots/xfce/">here</a>.<br/>
</p>
<hr/>

<h1>Documentation</h1>

<h2 id="thunar">Integration with the Thunar file manager</h2>
<p>
It is possible to create and extract archives using the thunar right-click menu.You need the Thunar-archive-plugin and a supported archive-manager like <a href="fileroller.sourceforge.net">File Roller</a>, <a href="http://xarchiver.xfce.org/">XArchiver</a> or <a href="http://squeeze.xfce.org/">Squeeze</a> to get this working.
</p>
<p>
	To learn more about the thunar-archive-plugin, look here:
	<a href="http://foo-projects.org/~benny/projects/thunar-archive-plugin/">http://foo-projects.org/~benny/projects/thunar-archive-plugin/</a>
</p>

<h2 id="roadmap">Roadmap</h2>
<ul>
<li>0.3:
	<ul>
	Add a settings dialog (to reduce cluttering of the main window)<br/>
	Custom Archive Action (to support special actions like integrity testing)<br/>
	Password Support (Bug #3072)<br/>
	Improve the User-Interface<br/>
	Support 7zip (Bug #2911)</br>
	</ul>
</li>
</ul>

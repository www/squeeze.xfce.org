<h1>Proposed icons</h1>
<p>
Here you can find all the icons submitted to <a href="http://bugzilla.xfce.org/show_bug.cgi?id=2763">Bug #2763</a>.
</p>
<p>
<table class="test">
<tr>
<th>16x16</th>
<th>48x48</th>
<th>Author</th>
<th>16x16</th>
<th>48x48</th>
<th>Author</th>
</tr>
<tr>
<td><img src="/images/icons/samuel/1/16x16.png"/></td>
<td><img src="/images/icons/samuel/1/48x48.png"/></td>
<td class="author">Samuel Verstraete</td>

<td><img src="/images/icons/samuel/2/16x16.png"/></td>
<td><img src="/images/icons/samuel/2/48x48.png"/></td>
<td class="author">Samuel Verstraete</td>
</tr>
<tr>
<td><img src="/images/icons/peter/16x16.png"/></td>
<td><img src="/images/icons/peter/48x48.png"/></td>
<td class="author">Peter de Ridder</td>

<td><img src="/images/icons/bjoern/1/16x16.png"/></td>
<td><img src="/images/icons/bjoern/1/48x48.png"/></td>
<td class="author">Björn Martenen</td>
</tr>
<tr>
<td><img src="/images/icons/radomir/16x16.png"/></td>
<td><img src="/images/icons/radomir/48x48.png"/></td>
<td class="author">Radomir Dopieralski</td>

<td><img src="/images/icons/bjoern/2/16x16.png"/></td>
<td><img src="/images/icons/bjoern/2/48x48.png"/></td>
<td class="author">Björn Martensen</td>
</tr>
<tr>
<td><img src="/images/icons/sebastian/16x16.png"/></td>
<td><img src="/images/icons/sebastian/48x48.png"/></td>
<td class="author">Sebastian Kraft</td>
</table>
</p>
<p>
The icons are copyright of their authors and are licensed under the GNU GPL.
</p>
<p class="written-by">Last Change: 03-06-2007</p>

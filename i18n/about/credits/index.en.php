<h1>Credits</h1>
<p>
Lead Developer<br/>
<ul>
Stephan Arts<br/>
</ul>
</p>
<p>
Contributors:<br/>
<ul>
Peter de Ridder<br/>
</ul>
</p>
<p>
Translators:
<ul>
	Alexander Nyakhaychyk (BE)<br/>
	Carles Muñz Gorriz (CA)<br/>
	Michal Várad (CS)<br/>
	Björn Martensen(DE) <br/>
	Stavros Giannouris (EL)<br/>
	Jeff Bailes (en_GB)<br/>
	Piarres Beobide (EU) <br/>
	Jari Rahkonen (FI)<br/>
	Maximilian Schleiss (FR)<br/>
	SZERVÁC Attila(HU)<br/>
	Stephan Arts (NL)<br/>
	A S Alam (PA)<br/>
	Besnik Bleta (SQ)<br/>
	Eren Türka (TR)<br/>
	Dmitry Nikitin (UK)<br/>
</ul>
</p>
<p>
And a special thanks to the users who contribute to squeeze by testing it out and report bugs when they find them.
</p>

<h1>About Squeeze</h1>
<p>
Squeeze is a modern and advanced archive manager for the Xfce Desktop Environment.<br/>
Its design adheres to the Xfce philosophy, which basically means Squeeze is designed to be both fast and easy to use.
<br/><br/>
Currently version <a href="/news#0.2.3-release">0.2.3</a> is the latest release of squeeze.
</p>

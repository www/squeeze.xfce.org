<div id="header">
	<img src="/images/header.png" height="120px" class="left" alt="Squeeze ... and archive management goes faster"/>
	<img src="/images/top-right.png" height="120px" class="right" alt=""/>
</div>
<div class="main-menu">
	<a href="/about" class="main-menu">About</a>
	<a href="/news" class="main-menu">News</a>
	<a href="/documentation" class="main-menu">Documentation</a>
	<a href="/screenshots" class="main-menu">Screenshots</a>
	<a href="http://archive.xfce.org/src/apps/squeeze/" class="main-menu">Download</a>
	<a href="/contact" class="main-menu">Contact</a>
</div>

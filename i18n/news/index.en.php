<h1>News</h1>
<h3 id="0.2.3-release">Squeeze 0.2.3 released</h3>
<p>
Squeeze 0.2.3 has been released, check the <a href="/downloads/ChangeLog-0.2.3">ChangeLog</a> for more information about this release.<br/>
It can be downloaded <a href="/download">here</a>.
</p>
<p class="written-by">Written by Stephan Arts - 24-02-2008</p>
<hr/>
<h3 id="0.2.2-release">Squeeze 0.2.2 released</h3>
<p>
Squeeze 0.2.2 has been released, check the <a href="/downloads/ChangeLog-0.2.2">ChangeLog</a> for more information about this release.<br/>
It can be downloaded <a href="/download">here</a>.
</p>
<p class="written-by">Written by Stephan Arts - 04-01-2008</p>
<hr/>
<h3 id="0.2.1-release">Squeeze 0.2.1 released</h3>
<p>
Squeeze 0.2.1 has been released, check the <a href="/downloads/ChangeLog-0.2.1">ChangeLog</a> for more information about this release.<br/>
It can be downloaded <a href="/download">here</a>.
</p>
<p class="written-by">Written by Stephan Arts - 25-04-2007</p>
<hr/>
<h3 id="0.2.0-release">Squeeze 0.2.0 released</h3>
<p>
Squeeze 0.2.0 has been released, this release incorporates many bugfixes. 
It can be downloaded <a href="/download">here</a>.<br/><br/>
<strong>NOTE:</strong><br/>
I advise everyone who is using version 0.1.0 to update to this version, since some critical stability issues have been solved.
</p>
<p class="written-by">Written by Stephan Arts - 15-04-2007</p>
<hr/>
<h3 id="dev-update-28-02-07">Update</h3>
<p>
Since the last release, the next version (0.2.0) is steadily evolving.<br/>
This is the status so far:
<ul>
	<u>libsqueeze</u>
	<li>Template structure for the support of new archives</li>
	<li>A better library interface to protect the internal datastructure</li>
	<li>A command queue has been implemented</li>
	<li>Number of signals has been reduced</li>
</ul>
<ul>
	<u>Squeeze</u>
	<li>Synchronisation issues with back-end are solved</li>
	<li>Some documentation is evolving (yes, a real user-guide)</li>
</ul>
</p>
<p class="written-by">Written by Stephan Arts - 28-02-2007</p>
<hr/>
<h3 id="0.1.0-release">Squeeze 0.1.0 released</h3>
<p>
Squeeze 0.1.0 has been released, this is the first release of the Squeeze archive manager.<br/>
Squeeze is a front-end to command-line archivers like tar, zip and rar.<br/>
It can be downloaded <a href="/download">here</a>.
</p>
<p class="written-by">Written by Stephan Arts - 29-01-2007</p>
<hr/>
<h3 id="needs-icon">Squeeze needs an icon</h3>
<p>
Squeeze is almost ready for its first release, but it does not have an icon yet.
<br/>
Are you an artist? Or just someone with a great idea for an icon?
<br/>
<br/>
If you want your icon to represent Squeeze, attach it to <a href="http://bugzilla.xfce.org/show_bug.cgi?id=2763">Bug #2763</a>.
<br/>
<br/>
The winning icon will be part of Squeeze 1.0.
</p>
<p class="written-by">Written by Stephan Arts - 16-01-2007</p>
<hr/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
$uri = $_SERVER["REDIRECT_URL"];
$uri = trim ($uri, '/');
$uri = strtolower($uri);

include ('functions/content.php');
$lang = "en";

print (	"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
print (	"<head>\n".
	"\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n".
	"<meta name=\"description\" content=\"Squeeze archive manager\" />".
	"<meta name=\"keywords\" ".
		"content=\"squeeze,".
		"archive manager,".
		"archiver,".
		"archive,".
		"open source,".
		"gtk+,".
		"archiving\" />".
	"<link rel=\"home\" href=\"/\" />".
	"<link rel=\"help\" href=\"/documentation/\" />".
	"<link rel=\"news\" href=\"/news/\" />".
	"<link rel=\"shortcut icon\" href=\"/favicon.png\" ".
		"type=\"image/png\" />".
	"\t<title>Squeeze</title>\n".
	"\t<link rel=\"stylesheet\" media=\"screen\" href=\"/layout.css\" type=\"text/css\" />\n".
	"</head>\n");
print (	"<body>\n" );
print (	"<div id=\"global-content\">\n" );
include ('i18n/header.'.$lang.'.php');

print (	"<div id=\"content\">\n" );

get_content($uri, $lang);

print ( "</div>");

include ('i18n/footer.'.$lang.'.php');
print ( "</div>\n");
print (	"</body>\n");
print (	"</html>\n");
?>

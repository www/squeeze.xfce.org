<?php

function get_content($location, $lang)
{
	if($location == "")
		$location = "about";
	if(is_dir('i18n/'.$location))
	{
		if(is_file('i18n/'.$location.'/nav.'.$lang.'php'))
			include('i18n/'.$location.'/nav.'.$lang.'.php');
		else
			include('i18n/'.$location.'/nav.en.php');

		if(is_file('i18n/'.$location.'/index.'.$lang.'php'))
			include('i18n/'.$location.'/index.'.$lang.'.php');
		else
			include('i18n/'.$location.'/index.en.php');
	}
	else
		include('i18n/404.en.php');
}


?>
